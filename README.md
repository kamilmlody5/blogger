# Blogger
Basic blog app with posts, users and sessions with EventStore and DDD

## Setup
```
bundle install
rake db:create db:migrate db:seed
rails s
```

## API examples
Import [api_postman.json](api_postman.json) to Postman app.
