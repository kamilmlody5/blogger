module EventStore
  module Types
    include Dry.Types

    Value = Types::Strict::Integer
    Email = Types::Strict::String.constrained(format: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i)
    Name = Types::Strict::String.constrained(min_size: 3)
    Password = Types::Strict::String.constrained(min_size: 7)
    AuthToken = Types::Strict::String.constrained(min_size: 32)
    UUID = Types::Strict::String.constrained(
      format: /\A[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}\z/i,
    )
    # ID = Types::Strict::Integer
    # Number = Integer.constrained(gt: 0)
    # Money = Dry::Types::Definition.new(::Money).constructor { |input| ::Money.from_amount(input.to_f, 'EU6') }
    # Age = Integer.constrained(gt: 18)
  end
end
