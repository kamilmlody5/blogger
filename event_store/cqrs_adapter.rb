module EventStore
  class CqrsAdapter
    attr_reader :event_store, :command_bus

    def initialize(event_store, command_bus)
      @event_store = event_store
      @command_bus = command_bus
    end

    def subscribe(subscriber, events)
      @event_store.subscribe(subscriber, to: events)
    end

    def subscribe_to_all_events(handler)
      @event_store.subscribe_to_all_events(handler)
    end

    def register(command, command_handler)
      @command_bus.register(command, command_handler)
    end

    def call(command)
      @command_bus.call(command)
    end
  end
end
