require 'aggregate_root'

module EventStore
  class AggregateRootRepository
    def initialize(event_store, notifications = ActiveSupport::Notifications)
      @repository = AggregateRoot::InstrumentedRepository.new(AggregateRoot::Repository.new(event_store), notifications)
    end

    def with_aggregate(aggregate_class, aggregate_id, &block)
      @repository.with_aggregate(
        aggregate_class,
        EventStore::AggregateRootRepository.stream_name(aggregate_class, aggregate_id),
        &block
      )
    end

    def self.stream_name(aggregate_class, aggregate_id)
      "#{aggregate_class.class.name}$#{aggregate_id}"
    end
  end
end
