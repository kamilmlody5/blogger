FactoryBot.define do
  factory :post, class: ::Posts::Repository::Record do
    id { SecureRandom.uuid }
    name { Faker::Restaurant.name }
    votes { [1, 2, 3].sample }
    user_id { nil }
  end
end
