FactoryBot.define do
  factory :user, class: ::Users::Repository::Record do
    id { SecureRandom.uuid }
    email { Faker::Internet.email }
    name { Faker::Name.name }
    password { Faker::Lorem.characters(number: 9) }
    password_digest { BCrypt::Password.create(password) }
    auth_token { SecureRandom.hex }
  end
end
