def response_json
  JSON.parse(response.body, symbolize_names: true)
end

def sign_in(user)
  Grape::Endpoint.before_each do |endpoint|
    allow(endpoint).to receive(:current_user).and_return(user)
    allow(endpoint).to receive(:authenticate!).and_return(nil)
  end
end

def sign_out
  Grape::Endpoint.before_each nil
end

def event_store
  Rails.configuration.event_store
end

def repository
  @repository ||= AggregateRoot::Repository.new(event_store)
end

def last_stream_event(stream_name)
  event_store.read.stream(stream_name).last
end
