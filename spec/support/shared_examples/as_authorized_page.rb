RSpec.shared_examples 'as authorized page' do |method, url|
  context 'when not logged in' do
    it 'return unauthorized response' do
      sign_out
      public_send(method, url)
      expect(response_json).to eq(error: 'You must be authenticated to do this action')
    end
  end
end
