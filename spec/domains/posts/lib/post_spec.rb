require 'rails_helper'

describe '::Posts::Post' do
  let!(:user)       { create :user }
  let!(:saved_post) { create :post, user_id: user.id }
  let(:new_post)    { build_stubbed :post, user_id: user.id }
  let(:stream_name) { ::EventStore::AggregateRootRepository.stream_name(subject.class.name, subject.post_id) }
  subject           { ::Posts::Post.new(post_id: post_id) }

  describe '#create_new_post' do
    let(:post_id) { new_post.id }

    context 'when params are valid' do
      it 'publish event PostCreated' do
        subject.create_new_post(user.id, new_post.name, new_post.votes)
        repository.store(subject, stream_name)

        expect(last_stream_event(stream_name).data).to eq({ post_id: new_post.id,
                                                            name: new_post.name,
                                                            votes: new_post.votes,
                                                            user_id: user.id })
      end
    end

    context 'when vote param is invalid' do
      let(:invalid_votes) { -2 }

      it 'raise error' do
        expect { subject.create_new_post(user.id, new_post.name, invalid_votes) }
          .to raise_error ::Posts::Post::InvalidVoteNumber
      end
    end
  end

  describe '#update_post' do
    let(:post_id) { saved_post.id }

    context 'when params are valid' do
      it 'publish event PostUpdated' do
        subject.update_post(user.id, new_post.name, new_post.votes)
        repository.store(subject, stream_name)

        expect(last_stream_event(stream_name).data).to eq({ post_id: saved_post.id,
                                                            name: new_post.name,
                                                            votes: new_post.votes,
                                                            user_id: user.id })
      end
    end

    context 'when vote param is invalid' do
      let(:invalid_votes) { -2 }

      it 'raise error' do
        expect { subject.update_post(user.id, new_post.name, invalid_votes) }
          .to raise_error ::Posts::Post::InvalidVoteNumber
      end
    end
  end

  describe '#delete_post' do
    context 'when params are valid' do
      let(:post_id) { saved_post.id }

      it 'publish event PostDeleted' do
        subject.delete_post(saved_post.id, user.id)
        repository.store(subject, stream_name)

        expect(last_stream_event(stream_name).data).to eq({ post_id: saved_post.id,
                                                            user_id: saved_post.user_id })
      end
    end
  end
end
