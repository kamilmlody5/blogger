require 'rails_helper'

describe '::Users::User' do
  let(:new_user)    { build_stubbed(:user) }
  let!(:saved_user) { create(:user) }
  let(:stream_name) { ::EventStore::AggregateRootRepository.stream_name(subject.class.name, subject.user_id) }
  subject           { ::Users::User.new(::Users::Repository, user_id: user_id) }

  describe '#create_new_user' do
    let(:user_id) { new_user.id }

    context 'when params are valid' do
      it 'publish event UserCreated' do
        subject.create_new_user(new_user.name, new_user.email, new_user.password_digest, new_user.auth_token)
        repository.store(subject, stream_name)

        expect(last_stream_event(stream_name).data).to eq({ auth_token: new_user.auth_token,
                                                            email: new_user.email,
                                                            name: new_user.name,
                                                            password_digest: new_user.password_digest,
                                                            user_id: subject.user_id })
      end
    end

    context 'when email already registered' do
      it 'raise error' do
        expect do
          subject.create_new_user(new_user.name, saved_user.email, new_user.password_digest, new_user.auth_token)
        end
          .to raise_error ::Users::User::EmailMustBeUnique
      end
    end
  end

  describe '#update_user' do
    let(:user_id) { saved_user.id }

    context 'when params are valid' do
      it 'publish event UserUpdated' do
        subject.update_user(saved_user.name, saved_user.email, new_user.password_digest, new_user.auth_token)
        repository.store(subject, stream_name)

        expect(last_stream_event(stream_name).data).to eq({ auth_token: new_user.auth_token,
                                                            email: saved_user.email,
                                                            name: saved_user.name,
                                                            password_digest: new_user.password_digest,
                                                            user_id: subject.user_id })
      end
    end
  end

  describe '#delete_user' do
    context 'when params are valid' do
      let(:user_id) { saved_user.id }

      it 'publish event UserUpdated' do
        subject.delete_user(saved_user.id)
        repository.store(subject, stream_name)

        expect(last_stream_event(stream_name).data).to eq({ user_id: subject.user_id })
      end
    end

    context 'when user not found' do
      let(:user_id) { new_user.id }

      it 'raise error' do
        expect { subject.delete_user(new_user.id) }.to raise_error ::Users::User::UserNotFound
      end
    end
  end
end
