require 'rails_helper'

describe '::Api::V1::Posts', type: :request do
  let!(:user) { create :user }
  let!(:post1) { create :post, user_id: user.id }
  let!(:post2) { create :post, user_id: user.id }
  let(:valid_params) { build_stubbed(:post, user_id: nil).attributes.with_indifferent_access }
  let(:invalid_params) { valid_params.merge(votes: -2) }

  describe 'GET #posts' do
    it_behaves_like 'as authorized page', 'get', '/api/v1/posts'

    context 'when logged in' do
      before do
        sign_in(user)
        get '/api/v1/posts'
      end

      it 'return posts array' do
        expect(response_json).to eq([{ id: post1.id, name: post1.name, votes: post1.votes },
                                     { id: post2.id, name: post2.name, votes: post2.votes }])
      end
    end
  end

  describe 'GET #post' do
    it_behaves_like 'as authorized page', 'get', '/api/v1/posts/123'

    context 'when logged in' do
      before do
        sign_in(user)
      end

      it 'return post info' do
        get "/api/v1/posts/#{post1.id}"

        expect(response_json).to eq({ id: post1.id, name: post1.name, votes: post1.votes,
                                      history: { name_changes: [] } })
      end
    end
  end

  describe 'POST #posts' do
    it_behaves_like 'as authorized page', 'post', '/api/v1/posts'

    context 'when logged in' do
      before { sign_in(user) }

      context 'when params valid' do
        subject { post '/api/v1/posts', params: valid_params }

        it 'return success info with post details' do
          subject
          expect(response_json).to include({ id: kind_of(String),
                                             name: valid_params[:name],
                                             votes: valid_params[:votes] })
        end

        it 'create a new record in DB' do
          expect { subject }.to change { ::Posts::Repository::Record.count }.by(1)
        end
      end

      context 'when params invalid' do
        subject { post '/api/v1/posts', params: invalid_params }

        it 'return error' do
          subject
          expect(response_json).to eq({ message: 'votes does not have a valid value', status: 422 })
        end

        it 'doesnt create a new post record' do
          expect { subject }.to_not change { ::Posts::Repository::Record.count }
        end
      end
    end
  end

  describe 'PUT #posts' do
    it_behaves_like 'as authorized page', 'put', '/api/v1/posts/abcd'

    context 'when logged in' do
      let(:updated_valid_params) { { name: 'asdf123', votes: 12 } }
      let(:updated_invalid_params) { { name: 'bcde123', votes: -5 } }

      before do
        # allow(::Posts::Repository).to receive(:find).with(post_id: post1.id, user_id: user.id).and_return post1
        sign_in(user)
      end

      context 'when params valid' do
        subject { put "/api/v1/posts/#{post1.id}", params: updated_valid_params }

        it 'return success' do
          subject
          expect(response_json).to include({ id: post1.id,
                                             name: updated_valid_params[:name],
                                             votes: updated_valid_params[:votes] })
        end
      end

      context 'when params invalid' do
        subject { put "/api/v1/posts/#{post1.id}", params: updated_invalid_params }

        it 'return error' do
          subject
          expect(response_json).to eq({ message: 'votes does not have a valid value', status: 422 })
        end
      end

      context 'when post not found' do
        subject { put '/api/v1/posts/abcd123', params: updated_valid_params }

        it 'return error' do
          subject
          expect(response_json).to eq({ message: 'id is invalid', status: 422 })
        end
      end
    end
  end

  describe 'DELETE #posts' do
    it_behaves_like 'as authorized page', 'put', '/api/v1/posts/abcd'

    context 'when logged in' do
      before { sign_in(user) }
      subject { delete "/api/v1/posts/#{post1.id}" }

      it 'return success message' do
        subject
        expect(response_json).to eq({ message: 'success' })
      end

      it 'removes post from DB' do
        subject
        expect(::Posts::Repository.find(post_id: post1.id, user_id: post1.id)).to be_nil
      end

      context 'when post not found' do
        subject { delete '/api/v1/posts/abcd123' }

        it 'return error' do
          subject
          expect(response_json).to eq({ message: 'id is invalid', status: 422 })
        end
      end
    end
  end
end
