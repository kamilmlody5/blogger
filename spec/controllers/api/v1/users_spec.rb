require 'rails_helper'

describe 'API::V1::Users', type: :request do
  let(:user) { build_stubbed :user }

  describe 'POST #signup' do
    let(:valid_params) { { name: 'abcd', email: 'sample@sample.com', password: 'abcd1234' } }
    let(:invalid_params) { valid_params.merge(email: 'abcds.pl') }
    subject { post '/api/v1/users', params: valid_params }

    context 'when logged in' do
      before { sign_in(user) }

      it 'return error about already logged in' do
        subject
        expect(response_json).to eq({ message: 'Already logged in', status: 401 })
      end
    end

    context 'when not logged in' do
      before { sign_out }
      context 'when params valid' do
        it 'return basic user info with token' do
          subject
          expect(response_json).to include({ email: valid_params[:email],
                                             id: kind_of(String),
                                             name: valid_params[:name],
                                             auth_token: kind_of(String) })
        end

        it 'create a new record in DB' do
          expect { subject }.to change { ::Users::Repository::Record.count }.by(1)
        end
      end

      context 'when params invalid' do
        subject { post '/api/v1/users', params: invalid_params }

        it 'return error' do
          subject
          expect(response_json).to eq({ message: 'email is invalid', status: 422 })
        end

        it 'dont create a new user in DB' do
          expect { subject }.to_not change { ::Users::Repository::Record.count }
        end
      end
    end
  end

  describe 'GET #me' do
    it_behaves_like 'as authorized page', 'get', '/api/v1/users/me'

    context 'when logged in' do
      before do
        sign_in(user)
        get '/api/v1/users/me'
      end

      it 'return user info' do
        expect(response_json).to eq({ email: user.email, id: user.id, name: user.name })
      end
    end
  end
end
