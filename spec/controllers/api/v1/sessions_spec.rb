require 'rails_helper'

describe 'API::V1::Sessions', type: :request do
  let!(:user) { create :user }

  describe 'POST' do
    let(:valid_params) { { email: user.email, password: user.password } }
    let(:invalid_params) { valid_params.merge(password: 'abcd') }

    context 'when email and password are valid' do
      subject { post '/api/v1/sessions', params: valid_params }

      it 'return valid user with auth_token' do
        subject
        expect(response_json).to include({ id: user.id,
                                           name: user.name,
                                           auth_token: kind_of(String) })
      end
    end

    context 'when email or password are invalid' do
      subject { post '/api/v1/sessions', params: invalid_params }

      it 'return error' do
        subject
        expect(response_json).to eq({ error: 'Invalid email or password' })
      end
    end
  end
end
