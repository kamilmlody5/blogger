module Posts
  module Services
    class ShowHistory
      def self.call(id, client, projection = ::RailsEventStore::Projection)
        projection
          .from_stream("Posts::Post$#{id}")
          .init(-> { { name_changes: [] } })
          .when(::Posts::Events::PostUpdated, ->(state, event) { state[:name_changes] << event.data[:name] })
          .run(client)

        # @history = client.read.
        #               stream('::Posts::PostCreated').
        #               backward.
        #               limit(10)
        # of_type([::Posts::PostCreated])
        # @history = client.read.backward.limit(100).to_a
      end
    end
  end
end
