module Posts
  module Presenters
    class WithHistory
      def self.call(post, history)
        ::Posts::Presenters::Basic.call(post).merge(
          {
            history: history,
          },
        )
      end
    end
  end
end
