module Posts
  module Presenters
    class Basic
      def self.call(post)
        {
          id: post.id,
          name: post.name,
          votes: post.votes,
        }
      end
    end
  end
end
