# subscriber list between rails_app and this domain -  ReadModel
module Posts
  class AppSubscribers
    def call(cqrs)
      cqrs.subscribe(
        ->(event) { ::Posts::Repository.create_or_update(event) },
        [::Posts::Events::PostCreated],
      )

      cqrs.subscribe(
        ->(event) { ::Posts::Repository.update(event) },
        [::Posts::Events::PostUpdated],
      )

      cqrs.subscribe(
        ->(event) { ::Posts::Repository.destroy(event) },
        [::Posts::Events::PostDeleted],
      )
    end
  end
end
