module Posts
  module Repository
    module Queries
      def find(post_id:, user_id: current_user.id)
        Record.find_by(id: post_id, user_id: user_id)
      end

      def find_or_initialize_by(id:)
        find(post_id: id) || ::Posts::Post.new(post_id: id)
      end

      def find_by_user_id(user_id)
        Record.where(user_id: user_id)
      end
    end
  end
end
