module Posts
  module Repository
    module Commands
      def save!(params)
        Record.create!(**params.to_h)
        nil
      end

      def create_or_update(event)
        post = Record.find_or_initialize_by(id: event.data.fetch(:post_id))
        post.user_id = event.data.fetch(:user_id)
        post.name = event.data.fetch(:name)
        post.votes = event.data.fetch(:votes)
        post.save!
      end

      def update(event)
        create_or_update(event)
      end

      def destroy(event)
        record = Record.find_by(id: event.data.fetch(:post_id))
        record.destroy
      end
    end
  end
end
