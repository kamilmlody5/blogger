# subscriber list for this domain only
module Posts
  class DomainSubscribers
    def call(cqrs)
      cqrs.register(Posts::Commands::CreatePost, Posts::EventHandlers::OnCreatePost.new(cqrs.event_store))
      cqrs.register(Posts::Commands::UpdatePost, Posts::EventHandlers::OnUpdatePost.new(cqrs.event_store))
      cqrs.register(Posts::Commands::DeletePost, Posts::EventHandlers::OnDeletePost.new(cqrs.event_store))
    end
  end
end
