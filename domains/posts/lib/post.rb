require 'aggregate_root'

module Posts
  class Post
    include AggregateRoot
    attr_reader :post_id, :user_id, :name, :votes

    InvalidVoteNumber = Class.new(StandardError)
    MustBeAnAuthor = Class.new(StandardError)

    def initialize(**attributes)
      @post_id, @user_id, @name, @votes = attributes.values_at(:post_id, :user_id, :name, :votes)
    end

    def create_new_post(user_id, name, votes)
      raise InvalidVoteNumber if votes <= 0

      apply ::Posts::Events::PostCreated.new(
        data: {
          post_id: @post_id,
          user_id: user_id,
          name: name,
          votes: votes,
        },
      )
    end

    def update_post(user_id, name, votes)
      raise InvalidVoteNumber if votes <= 0

      apply ::Posts::Events::PostUpdated.new(
        data: {
          post_id: @post_id,
          user_id: user_id,
          name: name,
          votes: votes,
        },
      )
    end

    def delete_post(post_id, user_id)
      apply ::Posts::Events::PostDeleted.new(
        data: {
          post_id: post_id,
          user_id: user_id,
        },
      )
    end

    on ::Posts::Events::PostCreated do |_event|
    end

    on ::Posts::Events::PostUpdated do |_event|
    end

    on ::Posts::Events::PostDeleted do |_event|
    end
  end
end
