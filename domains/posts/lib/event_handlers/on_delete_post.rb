module Posts
  module EventHandlers
    class OnDeletePost
      def initialize(event_store)
        @repository = ::EventStore::AggregateRootRepository.new(event_store)
      end

      def call(command)
        @repository.with_aggregate(::Posts::Post.new(post_id: command.post_id), command.post_id) do |post|
          post.delete_post(command.post_id, command.user_id)
        end
      end
    end
  end
end
