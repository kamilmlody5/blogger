module Posts
  module EventHandlers
    class OnCreatePost
      def initialize(event_store)
        @repository = ::EventStore::AggregateRootRepository.new(event_store)
      end

      def call(command)
        @repository.with_aggregate(::Posts::Post.new(post_id: command.post_id), command.post_id) do |post|
          post.create_new_post(command.user_id, command.name, command.votes)
        end
      end
    end
  end
end
