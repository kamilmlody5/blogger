module Posts
  module Commands
    class UpdatePost < ::EventStore::Command
      attribute :post_id, ::EventStore::Types::UUID
      attribute :user_id, ::EventStore::Types::UUID
      attribute :name, ::EventStore::Types::String
      attribute :votes, ::EventStore::Types::Value
    end
  end
end
