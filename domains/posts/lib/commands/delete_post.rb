module Posts
  module Commands
    class DeletePost < ::EventStore::Command
      attribute :post_id, ::EventStore::Types::UUID
      attribute :user_id, ::EventStore::Types::UUID
    end
  end
end
