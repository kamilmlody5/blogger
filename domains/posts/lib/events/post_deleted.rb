module Posts
  module Events
    class PostDeleted < ::EventStore::Event
      attribute :post_id, ::EventStore::Types::UUID
      attribute :user_id, ::EventStore::Types::UUID
    end
  end
end
