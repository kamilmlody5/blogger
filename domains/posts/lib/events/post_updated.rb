module Posts
  module Events
    class PostUpdated < ::EventStore::Event
      attribute :post_id, ::EventStore::Types::UUID
      attribute :user_id, ::EventStore::Types::UUID
      attribute :name, ::EventStore::Types::String
      attribute :votes, ::EventStore::Types::Value
    end
  end
end
