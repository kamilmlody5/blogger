module Posts
  module Repository
    class Record < ActiveRecord::Base
      self.table_name = 'posts'

      def author_id
        user_id
      end
    end

    class << self
      include Repository::Commands
      include Repository::Queries
    end
  end
end
