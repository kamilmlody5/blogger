module Users
  module Repository
    class Record < ActiveRecord::Base
      self.table_name = 'users'
      has_many :posts

      has_secure_password
    end

    class << self
      include Repository::Commands
      include Repository::Queries
    end
  end
end
