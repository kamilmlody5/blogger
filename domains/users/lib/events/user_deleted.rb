module Users
  module Events
    class UserDeleted < ::EventStore::Event
      attribute :user_id, ::EventStore::Types::UUID
    end
  end
end
