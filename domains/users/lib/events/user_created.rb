module Users
  module Events
    class UserCreated < ::EventStore::Event
      attribute :user_id, ::EventStore::Types::UUID
      attribute :name, ::EventStore::Types::Name
      attribute :email, ::EventStore::Types::Email
      attribute :password_digest, ::EventStore::Types::Password
      attribute :auth_token, ::EventStore::Types::AuthToken
    end
  end
end
