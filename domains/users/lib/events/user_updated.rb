module Users
  module Events
    class UserUpdated < ::EventStore::Event
      attribute :user_id, ::EventStore::Types::UUID
      attribute :name, ::EventStore::Types::Name
      attribute :email, ::EventStore::Types::Email
      attribute :password_digest, ::EventStore::Types::Password
    end
  end
end
