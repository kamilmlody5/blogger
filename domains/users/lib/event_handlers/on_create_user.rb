module Users
  module EventHandlers
    class OnCreateUser
      def initialize(event_store, user_repository)
        @repository = ::EventStore::AggregateRootRepository.new(event_store)
        @user_repository = user_repository
      end

      def call(command)
        @repository.with_aggregate(::Users::User.new(@user_repository, user_id: command.user_id),
                                   command.user_id) do |user|
          user.create_new_user(command.name,
                               command.email,
                               command.password_digest,
                               command.auth_token)
        end
      end
    end
  end
end
