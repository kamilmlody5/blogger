module Users
  module EventHandlers
    class OnUpdateUser
      def initialize(event_store)
        @repository = ::EventStore::AggregateRootRepository.new(event_store)
      end

      def call(command)
        @repository.with_aggregate(::Users::User.new(nil, user_id: command.user_id),
                                   command.user_id) do |user|
          user.update_user(command.name,
                           command.email,
                           command.password_digest,
                           command.auth_token)
        end
      end
    end
  end
end
