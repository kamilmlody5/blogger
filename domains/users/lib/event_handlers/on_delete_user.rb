module Users
  module EventHandlers
    class OnDeleteUser
      def initialize(event_store)
        @repository = ::EventStore::AggregateRootRepository.new(event_store)
      end

      def call(command)
        @repository.with_aggregate(::Users::User.new(nil, user_id: command.user_id),
                                   command.user_id) do |user|
          user.delete_user(command.user_id)
        end
      end
    end
  end
end
