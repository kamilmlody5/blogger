require 'aggregate_root'

module Users
  class User
    include AggregateRoot
    attr_reader :user_id, :name, :email, :password_digest, :auth_token

    EmailMustBeUnique = Class.new(StandardError)
    UserNotFound = Class.new(StandardError)

    def initialize(user_repository, **attributes)
      @user_id, @name, @email, @password_digest, @auth_token = attributes
                                                               .values_at(:user_id, :name, :email,
                                                                          :password_digest, :auth_token)
      @user_repository = user_repository
    end

    def create_new_user(name, email, password_digest, auth_token)
      raise EmailMustBeUnique if @user_repository.find_by_email(email).present?

      apply ::Users::Events::UserCreated.new(
        data: {
          user_id: @user_id,
          name: name,
          email: email,
          password_digest: password_digest,
          auth_token: auth_token,
        },
      )
    end

    def update_user(name, email, password_digest, auth_token)
      apply ::Users::Events::UserUpdated.new(
        data: {
          user_id: @user_id,
          name: name,
          email: email,
          password_digest: password_digest,
          auth_token: auth_token,
        },
      )
    end

    def delete_user(user_id)
      raise UserNotFound unless @user_repository.find(id: @user_id).present?

      apply ::Users::Events::UserDeleted.new(
        data: {
          user_id: user_id,
        },
      )
    end

    on(::Users::Events::UserCreated) { |_event| }

    on(::Users::Events::UserUpdated) { |_event| }

    on(::Users::Events::UserDeleted) { |_event| }
  end
end
