# subscriber list for this domain only
module Users
  class DomainSubscribers
    def initialize(user_repository)
      @user_repository = user_repository
    end

    def call(cqrs)
      cqrs.register(Users::Commands::CreateUser,
                    Users::EventHandlers::OnCreateUser.new(cqrs.event_store, @user_repository))
      cqrs.register(Users::Commands::UpdateUser, Users::EventHandlers::OnUpdateUser.new(cqrs.event_store))
      cqrs.register(Users::Commands::DeleteUser, Users::EventHandlers::OnDeleteUser.new(cqrs.event_store))
    end
  end
end
