module Users
  module Commands
    class DeleteUser < ::EventStore::Command
      attribute :user_id, ::EventStore::Types::UUID
    end
  end
end
