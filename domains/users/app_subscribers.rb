# subscriber list between rails_app and this domain
module Users
  class AppSubscribers
    def call(cqrs)
      cqrs.subscribe(
        ->(event) { ::Users::Repository.create_or_update(event) },
        [::Users::Events::UserCreated],
      )

      cqrs.subscribe(
        ->(event) { ::Users::Repository.update(event) },
        [::Users::Events::UserUpdated],
      )

      cqrs.subscribe(
        ->(event) { ::Users::Repository.destroy(event) },
        [::Users::Events::UserDeleted],
      )
    end
  end
end
