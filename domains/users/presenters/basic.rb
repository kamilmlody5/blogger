module Users
  module Presenters
    class Basic
      def self.call(user)
        {
          id: user.id,
          name: user.name,
          email: user.email,
        }
      end
    end
  end
end
