module Users
  module Presenters
    class WithAuth
      def self.call(user)
        ::Users::Presenters::Basic.call(user)
                                  .merge(
                                    {
                                      auth_token: user.auth_token,
                                    },
                                  )
      end
    end
  end
end
