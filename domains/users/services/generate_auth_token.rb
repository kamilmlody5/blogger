module Users
  module Services
    class GenerateAuthToken
      HEX_SIZE = 32

      def self.call
        SecureRandom.hex(HEX_SIZE)
      end
    end
  end
end
