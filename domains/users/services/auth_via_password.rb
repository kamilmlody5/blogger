module Users
  module Services
    class AuthViaPassword
      def self.call(user_id, password, repository = ::Users::Repository)
        !!repository.find(id: user_id).authenticate(password)
      end
    end
  end
end
