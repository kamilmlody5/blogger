module Users
  module Repository
    module Commands
      def save!(params)
        Record.create!(**params.to_h)
        nil
      end

      def create_or_update(event)
        user = Record.find_or_initialize_by(id: event.data.fetch(:user_id))
        user.name = event.data.fetch(:name)
        user.email = event.data.fetch(:email)
        user.password_digest = event.data.fetch(:password_digest)
        user.auth_token = event.data.fetch(:auth_token)
        user.save!
      end

      def update(event)
        create_or_update(event)
      end

      def destroy(event)
        record = Record.find_by(id: event.data.fetch(:user_id))
        record.destroy
      end
    end
  end
end
