module Users
  module Repository
    module Queries
      def find(id:)
        Record.find_by(id: id)
      end

      def find_or_initialize_by(id:)
        find(id: id) || ::Users::User.new(self, user_id: id)
      end

      def find_by_auth_token(auth_token)
        Record.find_by(auth_token: auth_token)
      end

      def find_by_email(email)
        Record.find_by(email: email)
      end
    end
  end
end
