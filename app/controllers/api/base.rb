module Api
  class Base < Grape::API
    mount Api::V1::Base
    add_swagger_documentation base_path: '/',
                              mount_path: '/swagger_doc',
                              doc_version: '1.0',
                              add_version: false,
                              hide_documentation_path: true,
                              host: 'localhost:3000',
                              security_definitions: {
                                ApiKeyAuth:{
                                  type: "apiKey",
                                  name: "Auth-Token",
                                  in: "header",
                                  description: "Requests should pass an Auth Token header."
                                }
                              },
                              info: {
                                title: 'Blogger API Documentation',
                                description: "Documentation of Blogger App",
                                # contact_name: "Kamil",
                                # contact_email: "kamil@kamil",
                                # contact_url: "kamil-website",
                                # license: "The name of the license.",
                                # license_url: "www.The-URL-of-the-license.org",
                                # terms_of_service_url: "www.The-URL-of-the-terms-and-service.com",
                              }
  end
end
