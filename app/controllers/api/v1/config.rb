module Api
  module V1
    module Config
      extend ActiveSupport::Concern

      included do
        prefix 'api'
        version 'v1', using: :path
        default_format :json
        format :json

        helpers do
          def permitted_params
            @permitted_params ||= declared(params, include_missing: false)
          end

          def logger
            Rails.logger
          end

          def event_store
            Rails.configuration.event_store
          end

          def command_bus
            Rails.configuration.command_bus
          end

          def current_user
            @current_user ||= ::Users::Repository.find_by_auth_token(headers['Auth-Token'])
          end

          def authenticate!
            error!('You must be authenticated to do this action', 401) unless headers['Auth-Token']
            error!('Please pass valid Auth-Token', 401) unless current_user
          end
        end

        rescue_from Grape::Exceptions::ValidationErrors do |e|
          error!(message: e.message, status: 422)
        end

        rescue_from ActiveRecord::RecordNotFound do |_|
          error!(message: 'Resource not found', status: 404)
        end

        rescue_from ActiveRecord::RecordInvalid do |e|
          error!(message: e.message, status: 422)
        end
      end
    end
  end
end
