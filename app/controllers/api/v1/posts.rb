module Api
  module V1
    class Posts < Base
      resources :posts do
        before { authenticate! }

        desc 'Get all posts'
        get do
          posts = ::Posts::Repository.find_by_user_id(current_user.id)
          posts.map { |post| ::Posts::Presenters::Basic.call(post) }
        end

        desc 'Get post by id'
        params do
          requires :id, type: Types::Uuid
        end
        get '/:id' do
          post = ::Posts::Repository.find(post_id: params[:id], user_id: current_user.id)
          raise ActiveRecord::RecordNotFound unless post.present?

          history = ::Posts::Services::ShowHistory.call(post.id, event_store)

          ::Posts::Presenters::WithHistory.call(post, history)
        end

        desc 'Create a new post'
        params do
          requires :name, type: String
          requires :votes, type: Integer, default: 0, values: 0..99
        end
        post do
          uuid = SecureRandom.uuid
          cmd = ::Posts::Commands::CreatePost.new(post_id: uuid,
                                                  user_id: current_user.id,
                                                  name: params[:name],
                                                  votes: params[:votes])
          command_bus.call(cmd)

          post = ::Posts::Repository.find(post_id: uuid, user_id: current_user.id)

          ::Posts::Presenters::Basic.call(post)
        end

        desc 'Update new post'
        params do
          requires :id, type: Types::Uuid
          optional :name, type: String
          optional :votes, type: Integer, values: 0..99
        end
        put '/:id' do
          post = ::Posts::Repository.find(post_id: params[:id], user_id: current_user.id)

          raise ActiveRecord::RecordNotFound unless post.present?

          cmd = ::Posts::Commands::UpdatePost.new(post_id: post.id,
                                                  user_id: current_user.id,
                                                  name: params[:name].presence || post.name,
                                                  votes: params[:votes].presence || post.votes)
          command_bus.call(cmd)
          post = ::Posts::Repository.find(post_id: params[:id], user_id: current_user.id)
          ::Posts::Presenters::Basic.call(post)
        end

        desc 'Delete a post'
        params do
          requires :id, type: Types::Uuid
        end
        delete '/:id' do
          post = ::Posts::Repository.find(post_id: params[:id], user_id: current_user.id)

          raise ActiveRecord::RecordNotFound unless post.present?

          cmd = ::Posts::Commands::DeletePost.new(post_id: post.id,
                                                  user_id: current_user.id)
          command_bus.call(cmd)
          { message: 'success' }
        end
      end
    end
  end
end
