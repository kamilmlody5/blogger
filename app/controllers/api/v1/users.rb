module Api
  module V1
    class Users < Base
      resources :users do
        desc 'Signup' do
          http_codes [
                       { code: 201, message: 'Success' },
                       { code: 422, message: 'Email must be unique' },
                       { code: 401, message: 'Already logged in' }
                     ]
          summary 'Create a new user account'
          detail 'Here you can create a new account'
          nickname 'Create User'
          # deprecated false
          # hidden false
        end
        params do
          requires :name, type: Types::Name, desc: 'Full name',
                   documentation: { example: 'Full Name', type: String }
          requires :email, type: Types::Email, desc: 'Your Email',
                   documentation: { example: 'sample@mail.com', type: String }
          requires :password, type: Types::Password, desc: 'Your password',
                   documentation: { example: 'p$ssw0rd213!', type: String }
        end
        post do
          error!(message: 'Already logged in', status: 401) if current_user

          uuid = SecureRandom.uuid
          password_digest = BCrypt::Password.create(params[:password])

          raise ::Users::User::EmailMustBeUnique if ::Users::Repository.find_by_email(params[:email]).present?

          cmd = ::Users::Commands::CreateUser.new(user_id: uuid,
                                                  name: params[:name],
                                                  email: params[:email],
                                                  password_digest: password_digest,
                                                  auth_token: ::Users::Services::GenerateAuthToken.call)
          command_bus.call(cmd)
          user = ::Users::Repository.find(id: uuid)
          ::Users::Presenters::WithAuth.call(user)
        rescue ::Users::User::EmailMustBeUnique
          error!(message: 'Email must be unique', status: 422)
        end

        namespace do
          before { authenticate! }

          desc 'Get user info' do
            http_codes [
                         { code: 201, message: 'Success' },
                         { code: 401, message: 'You must be authenticated to do this action' },
                         { code: 401, message: 'Please pass valid Auth-Token' },
                       ]
            summary 'Get user information'
            detail 'Get information about current logged in user'
            headers 'Auth-Token': { description: 'Validates your identity', required: true }
            nickname 'Get User Info'
          end

          get 'me' do
            ::Users::Presenters::Basic.call(current_user)
          end
        end
      end
    end
  end
end
