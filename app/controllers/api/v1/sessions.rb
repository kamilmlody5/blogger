module Api
  module V1
    class Sessions < Base
      resources :sessions do
        desc 'Get authentication token via email and password'
        params do
          requires :email, type: Types::Email
          requires :password, type: String
        end
        post do
          user = ::Users::Repository.find_by_email(params[:email])

          error!('Invalid email or password', 401) unless user.present?
          error!('Invalid email or password', 401) unless ::Users::Services::AuthViaPassword
                                                          .call(user.id, params[:password])

          cmd = ::Users::Commands::UpdateUser.new(user_id: user.id,
                                                  name: user.name,
                                                  email: user.email,
                                                  password_digest: user.password_digest,
                                                  auth_token: ::Users::Services::GenerateAuthToken.call)
          command_bus.call(cmd)
          user = ::Users::Repository.find_by_email(params[:email])

          ::Users::Presenters::WithAuth.call(user)
        end
      end
    end
  end
end
