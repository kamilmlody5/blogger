module Api
  module V1
    class Base < Grape::API
      include Api::V1::Config

      mount Api::V1::Users
      mount Api::V1::Sessions
      mount Api::V1::Posts
    end
  end
end
