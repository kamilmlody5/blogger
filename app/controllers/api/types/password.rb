module Api
  module Types
    class Password
      class << self
        def parse(value)
          value
        end

        def parsed?(value)
          ::EventStore::Types::Password[value]
        rescue StandardError
          false
        end
      end
    end
  end
end
