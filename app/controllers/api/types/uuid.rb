module Api
  module Types
    class Uuid
      class << self
        def parse(value)
          value
        end

        def parsed?(value)
          ::EventStore::Types::UUID[value]
        rescue StandardError
          false
        end
      end
    end
  end
end
