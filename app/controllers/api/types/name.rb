module Api
  module Types
    class Name
      class << self
        def parse(value)
          value
        end

        def parsed?(value)
          ::EventStore::Types::Name[value]
        rescue StandardError
          false
        end
      end
    end
  end
end
