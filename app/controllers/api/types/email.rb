module Api
  module Types
    class Email
      class << self
        def parse(value)
          value
        end

        def parsed?(value)
          ::EventStore::Types::Email[value]
        rescue StandardError
          false
        end
      end
    end
  end
end
