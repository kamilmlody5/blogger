command_bus = Rails.configuration.command_bus
ActiveRecord::Base.transaction do
  # create user
  user_uuid = SecureRandom.uuid
  auth_token = '315323730330c3ccd803aee9b8cd905c689a3ad941b6c9dad66c9b2c86ce4c5f'
  password_digest = ::Users::Repository::Record.new(password: '12345678a!').password_digest

  command_bus.call(
    ::Users::Commands::CreateUser.new(user_id: user_uuid, # this command will also create 3 posts
                                      name: 'User Name',
                                      email: 'email@sample.com',
                                      password_digest: password_digest,
                                      auth_token: auth_token),
  )

  # create posts
  ['Seeds The great tale', 'Seeds My opinions', 'Seeds Stopping things', 'Seeds Post Sample 2'].each do |name|
    command_bus.call(
      ::Posts::Commands::CreatePost.new(post_id: SecureRandom.uuid,
                                        name: name,
                                        user_id: user_uuid,
                                        votes: [1, 2, 3].sample),
    )
  end

  command_bus.call(
    ::Posts::Commands::UpdatePost.new(post_id: ::Posts::Repository::Record.last(2).first.id,
                                      name: 'Updated post title by CommandBus',
                                      user_id: user_uuid,
                                      votes: [1, 2, 3].sample),
  )

  command_bus.call(
    ::Posts::Commands::DeletePost.new(post_id: ::Posts::Repository::Record.last.id, user_id: user_uuid),
  )
end

