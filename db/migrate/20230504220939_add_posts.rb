class AddPosts < ActiveRecord::Migration[7.0]
  def change
    create_table :posts, id: :uuid do |t|
      t.string :name
      t.integer :votes
      t.references :user, null: false, type: :uuid, index: true
      t.timestamps
    end
  end
end
