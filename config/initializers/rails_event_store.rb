require "rails_event_store"
require "arkency/command_bus"
Dir["#{Rails.root}/event_store/*.rb"].each { |file| require file }

module Blogger
  Rails.configuration.to_prepare do
    Rails.configuration.event_store = if Rails.env.test?
                                        RailsEventStore::Client.new(repository: RubyEventStore::InMemoryRepository.new)
                                      else
                                        RailsEventStore::Client.new(
                                          repository: RailsEventStoreActiveRecord::EventRepository
                                                        .new(serializer: RubyEventStore::NULL)
                                        )
                                      end
    Rails.configuration.command_bus = Arkency::CommandBus.new
    cqrs = EventStore::CqrsAdapter.new(Rails.configuration.event_store, Rails.configuration.command_bus)

    GlobalSubscribers.call(cqrs)
  end
end
