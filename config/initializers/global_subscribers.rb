# Global subscribers for the whole app(glue all domains)
class GlobalSubscribers
  class << self
    def call(cqrs)
      link_rails_event_store(cqrs)

      user_repository = ::Users::Repository
      [
        ::Posts::DomainSubscribers.new,
        ::Posts::AppSubscribers.new,
        ::Users::DomainSubscribers.new(user_repository),
        ::Users::AppSubscribers.new
      ].each { |c| c.call(cqrs) }

      generate_sample_posts_when_user_created(cqrs)
    end

    private

    def generate_sample_posts_when_user_created(cqrs)
      cqrs.subscribe(
        ->(event) do
          ["The great tale", "My opinions", "Stopping things", "Post Samples Content"].each do |name|
            cqrs.call(
              ::Posts::Commands::CreatePost.new(post_id: SecureRandom.uuid,
                                                name: "#{name} - post auto created after UserCreated event",
                                                user_id: event.data.fetch(:user_id),
                                                votes: [1, 2, 3].sample)
            )
          end
        end,
        [::Users::Events::UserCreated]
      )
    end

    def link_rails_event_store(cqrs)
      [
        RailsEventStore::LinkByEventType.new,
        RailsEventStore::LinkByCorrelationId.new,
        RailsEventStore::LinkByCausationId.new
      ].each { |h| cqrs.subscribe_to_all_events(h) }
    end
  end
end
