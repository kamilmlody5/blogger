Rails.application.routes.draw do
  mount Api::Base, at: "/"
  mount RailsEventStore::Browser => '/res' if Rails.env.development?
end
